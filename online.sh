#! /bin/bash

xhost local:root

output_dir=$1
mkdir -p $output_dir
output_dir=$(realpath $output_dir)

docker run \
    --gpus all \
    --env="DISPLAY" \
    --net=host \
    --device=/dev/video0:/dev/video0 \
    --mount type=bind,source=$HOME/.Xauthority,target=/root/.Xauthority:rw \
    --mount type=bind,source=$output_dir,target=/output \
    -it \
    registry.eps.surrey.ac.uk/openpose:15668 \
    /bin/bash -c \
    "./build/examples/openpose/openpose.bin \
    --write_json /output \
    --number_people_max 1 \
    --hand"
