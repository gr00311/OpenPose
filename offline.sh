#! /bin/bash
video=$(realpath $1)
video_dir=$(dirname $video)
video_name=$(basename $video)

output_tar=$2
output_dir=$(dirname $output_tar)
mkdir -p $output_dir
output_dir=$(realpath $output_dir)
output_name=$(basename $output_tar)
tmp_name=$(basename $output_tar | cut -d. -f1)

docker run \
    --gpus all \
    --mount type=bind,source=$video_dir,target=/input \
    --mount type=bind,source=$output_dir,target=/output \
    --user $(id -u):$(id -g) \
    registry.eps.surrey.ac.uk/openpose:15668 \
    /bin/bash -c \
    "mkdir -p /tmp/$tmp_name && \
    ./build/examples/openpose/openpose.bin \
    --video /input/$video_name \
    --write_json /tmp/$tmp_name \
    --number_people_max 1 \
    --scale_number 4 \
    --scale_gap 0.25 \
    --hand \
    --hand_scale_number 6 \
    --hand_scale_range 0.4 \
    --display 0 \
    --render_pose 0 && \
    cd /tmp && \
    tar -cf /output/$output_name $tmp_name"
