#! /bin/bash

xhost local:root

video=$(realpath $1)
video_dir=$(dirname $video)
video_name=$(basename $video)

output_tar=$2
output_dir=$(dirname $output_tar)
mkdir -p $output_dir
output_dir=$(realpath $output_dir)
output_name=$(basename $output_tar)
tmp_name=$(basename $output_tar | cut -d. -f1)

docker run \
    --gpus all \
    --env="DISPLAY" \
    --net=host \
    --mount type=bind,source=$HOME/.Xauthority,target=/root/.Xauthority:rw \
    --mount type=bind,source=$video_dir,target=/input \
    --mount type=bind,source=$output_dir,target=/output \
    --user $(id -u):$(id -g) \
    registry.eps.surrey.ac.uk/openpose:15668 \
    /bin/bash -c \
    "mkdir -p /tmp/$tmp_name && \
    ./build/examples/openpose/openpose.bin \
    --video /input/$video_name \
    --write_json /tmp/$tmp_name \
    --number_people_max 1 \
    --hand && \
    cd /tmp && \
    tar -cf /output/$output_name $tmp_name"
